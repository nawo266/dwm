# My DWM build

![Screenshot of my desktop](https://gitlab.com/nawo266/dwm/-/raw/master/dwm.jpg) 

## Used patches:
+ dwm-actualfullscreen-20211013-cb3f58a.diff
+ dwm-alpha-systray-6.3_full.diff
+ dwm-cfacts-vanitygaps-6.3_full.diff
+ dwm-cyclelayouts-6.3.diff
+ dwm-fsignal-6.2.diff
+ dwm-pertag-20200914-61bb8b2.diff
+ dwm-preserveonrestart-6.3.diff
+ dwm-resizecorners-6.2.diff
+ dwm-restartsig-20180523-6.2.diff
+ dwm-rotatestack-20161021-ab9571b.diff
+ dwm-statuscmd-20210405-67d76bd.diff
+ dwm-steam-6.2.diff
+ dwm-swallow-6.3.diff
+ dwm-winicon-6.3-v2.1.diff
+ dwm-xrdb-6.4.diff
